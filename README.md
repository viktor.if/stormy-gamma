## System Requirements

System or Functional Requirements tell what the system must do and what users must be able to do with the software.

E.g.:

1. The system shall **assign** a unique tracking number to each shipment.

2. The system shall **display** the due date of payment as MM/DD/YYYY.

## Non-functional Requirements

Non-functional Requirements or Quality Attributes (QA) tell about the software's expected perfomance behaviour such as:

E.g. With 100 concurrent users, a database record shall be fetched over the network in **less than three milliseconds**.

Common QAs:

- **Efficiency** is how the software will use the resources (e.g., disk space, ram, bandwidt)

- **Flexebility** is how to easy or hard it is to add a new feature to the software

- **Integrity** or **Data Integrity** defines data quality which guarantees the data is complete and has a whole structure

- **Interoperability** is exchanging data or services with other software systems

- **Usability** when the system is easy to use, user-friendly, the interface is appealing, and documentation is understandable

- **Maintainability** is how easy and fast it is to fix a particular bug or add a new feature

- **Portability** is the effort required to move or migrate software from one platform to another

- **Reusability** is an attribute in which software, or its module is reused with very little or no modification

- **Recoverability** is the ability to restore your deployment to the point at which a failure occurred

- **Testability** is whether or not the software and integrated products can be tested

- **Availability** is the propability that a system will be operationaland able to deliver the requested services at a point in time

- **Reliability** refers to the probability that the system will meet specific perfomance standarts while providing correct output for the desired time duration

- **Robustness** is the degree to which the system continues to function correctly when encountered with invalid data

## User Classes and their characteristics

The product might be used more than one class of users.

User classes may be differentiated based on _frequency of use_, _subset of product functions used_, _technical expertise_, _security or privilege levels_, _educational level_, or _experience_.

Commonly, the pertinent characteristics of each user class are described. Certain requirements may pertain only to certain user classes.

## Constraints, Assumptions, Dependencies and their roles

**Constraints**. There might be a number of constraints which the system must abide by during development.

E.g.:

- Regulatory policies

- Hardware limitations

- Interface of other applications

- Safety and security considerations

**Assumptions** and **Dependencies** are factors that affect the requirements stated in the SRS. These factors are not design constraints on the software but are, rather, any changes to them that can affect the requirements in the SRS. 

E.g. An assumption might be that a specific operating system would be available on the hardware designated for the software product. If, in fact, the operating system were not available, the SRS would then have to change accordingly.

## Requirements artifacts

An **artifact** is a byproduct of software development that helps describe the architecture, design and function of software.

**Product vision** or the **Product Vision Statement** is a description of the essence of your product: what are the problems it is solving, for whom, and why now is the right time to build it. A Product vision gives your team a bigger picture of what they are working on and why.

E.g. for Microsoft Surface:

- For the business user who needs to be productive in the office and on the go, the Surface is a convertible tablet that is easy to carry and gives you full computing productivity no matter where you are.

- Unlike laptops, Surface serves your on-the-go needs without having to carry an extra device.

A **Scope Document** is simply a piece of formal documentation outlining both product scope and project scope.

## The Root Causes of Project Success and Failure

#### Main factors of project failure:

1. Incomplete requirements

2. Lack of user involment

3. Lack of resources

4. Unrealistic expectations

5. Lack of executive support

6. Changing requirements and specifications

7. Lack of planning

8. Didn't need it any more

9. Lack of management

10. Technical illiteracy

#### Top factors of project success:

1. User involment

2. Executive management support

3. Clear statement of requiremetns

4. Proper planning

5. Realistic expectation

## Project Scope vs. Product Scope

#### Product Scope

The **Product Scope** is the features and functions that characterize a product, service, or result.

In short, the **Product Scope** is about the product details; what the product will look like, how it will work, its features, and more.

#### Project Scope

The **Project Scope** is the work performed to deliver a product, service, or result with the specified features and functions.

In short, the **Project Scope** defines the requirements of the product and the work required to create it. This also defines what is inside and outside of the scope, which helps you avoid scope creep.

In summary, **Product Scope** is oriented towards the “what” (functional requirements), while **Project Scope** is oriented towards the “how” (work-related).


## BA Vs. PM responsibilities

|Project manager’s responsibilities|Business analyst’s responsibilities  |
|:---------------------------------|:------------------------------------|
|Creating the project charter and defining the project scope, objectives, goals, and vision for the project (in conjunction with the project sponsor)|Creating the business analysis approach: Making tailoring decisions about how to complete the analysis required from planning the BA approach to creating estimates for the required work|
|Creating a project plan for how the work will be done including a project schedule|Eliciting requirements relevant to business problems and engaging with stakeholders at all levels|
|Reporting progress to the sponsor and steering group/project board or to the program manager|Managing and prioritizing business requirements with input from stakeholders|
|Leading on risk management and dealing with issues (although some tasks may be carried out by subject matter experts, the PM is the guardian of the process)|Ensuring that what’s built is what was requested and providing input into testing and validation of requirements|
|Leading the team, engaging team members and stakeholders at all levels and project communications|

## Requirements prioritization

#### Requirement prioritization techniques:

* Ranking

* Numeric Assignment

* MoScoW Technique

* Buble Sort Technique

* Hundred Dollar Method

* Five Whys

## C4 UML Diagrams

1. System Context level
2. Container level
3. Components level
4. Code level

E.g. C4 diagrams for Vitaance

1. System Context diagram

![System Context diagram for Vitaance](./diagrams/system-context-diagram.png)

2. Container diagram

![Container diagram for Vitaance](./diagrams/container-diagram.png)

3. Component diagram

![Component diagram for Vitaance](./diagrams/component-diagram.png)

